package Mapper;
import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import au.com.bytecode.opencsv.CSVParser;


/**
 * Mapper that does the following:
 * 1. Load line from csv file
 * 2. Clean each data
 * 3. Combine title and body, and return it as a key
 * 4. Return tag as a value
 * @author develover
 *
 */
public class CSVLoader extends Mapper<LongWritable, Text, Text, Text>{
	public void map(LongWritable key, Text value, Context context)
		throws IOException, InterruptedException{
		System.out.println("In CSVLoader");
		String line = value.toString();
		String[] fields = new CSVParser().parseLine(line);
		
		/*
		 * Cleaning part to be implemented later
		 */
		
		String buf = fields[1] + fields[2];
		context.write(new Text(buf), new Text(fields[3]));
	}
}
