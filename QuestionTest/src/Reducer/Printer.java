package Reducer;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;


/**
 * Reducer that aggregates the result from mapper and prints them.
 * @author develover
 *
 */
public class Printer extends Reducer<Text, Text, Text, Text>{
	public void reduce(Text key, Text value, Context context)
		throws IOException, InterruptedException{
		System.out.println("In Printer");
		context.write(key, value);
	}
}
